<?php

/**
 * @file
 * Contains SearchApiNoindexAlterNoindexFilter.
 */

/**
 * Represents a data alteration that restricts entity indexes to some nodes.
 */
class SearchApiNoindexAlterNoindexFilter extends SearchApiAbstractAlterCallback {

  /**
   * Overrides SearchApiAbstractAlterCallback::supportsIndex().
   *
   * Returns TRUE for indexes on supported entity types.
   */
  public function supportsIndex(SearchApiIndex $index) {
    return in_array($index->getEntityType(), search_api_noindex_supported_entity_types());
  }

  /**
   * Overrides SearchApiAbstractAlterCallback::alterItems().
   *
   * Removes nodes with Noindex status.
   */
  public function alterItems(array &$items) {
    foreach ($items as $entity_id => $entity) {
      if (isset($entity->search_api_noindex['exclude']) && $entity->search_api_noindex['exclude']) {
        unset($items[$entity_id]);
      }
    }
  }
}
