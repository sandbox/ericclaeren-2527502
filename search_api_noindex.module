<?php

/**
 * @file
 * Removes specified nodes from any selected search index.
 */

/**
 * Implements hook_permission().
 */
function search_api_noindex_permission() {
  return array(
    'searchnoindex exclude nodes' => array(
      'title' => t('Exclude nodes from the search index'),
    ),
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function search_api_noindex_form_node_form_alter(&$form, &$form_state, $form_id) {
  $node = $form['#node'];

  if (search_api_noindex_is_enabled($node->type)) {
    $excluded = search_api_noindex_has_entity($node);
    $form['search_api_noindex'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#group' => 'additional_settings',
      '#title' => t('Internal search options'),
      '#access' => user_access('searchnoindex exclude nodes'),
    );
    $form['search_api_noindex']['exclude'] = array(
      '#type' => 'checkbox',
      '#default_value' => $excluded,
      '#required' => FALSE,
      '#title' =>  t('Exclude this node from the search index.'),
    );
  }
}

/**
 * Implements hook_entity_load().
 */
function search_api_noindex_entity_load($entities, $type) {
  if (search_api_noindex_supported($type)) {
    $search_api_noindex_entity_ids = search_api_noindex_get_ids(array_keys($entities), $type);
    foreach ($entities as $entity) {
      list($id, $vid, $bundle) = entity_extract_ids($type, $entity);
      $entity->search_api_noindex['exclude'] = in_array($id, $search_api_noindex_entity_ids);
    }
  }
}

/**
 * Implements hook_entity_insert().
 */
function search_api_noindex_entity_insert($entity, $type) {
  search_api_noindex_entity_update($entity, $type);
}

/**
 * Implements hook_entity_update().
 */
function search_api_noindex_entity_update($entity, $type) {
  if (isset($entity->search_api_noindex['exclude'])) {
    if ($entity->search_api_noindex['exclude']) {
      search_api_noindex_insert($entity, $type);
    }
    else {
      search_api_noindex_delete($entity, $type);
    }
  }
}

/**
 * Implements hook_entity_delete().
 */
function search_api_noindex_entity_delete($entity, $type) {
  search_api_noindex_delete($entity, $type);
}

/**
 * Implements hook_search_api_alter_callback_info().
 */
function search_api_noindex_search_api_alter_callback_info() {
  $callbacks['search_api_noindex_alter_noindex_filter'] = array(
    'name' => t('Noindex filter'),
    'description' => t('Exclude items from indexing based on their noindex setting.'),
    'class' => 'SearchApiNoindexAlterNoindexFilter',
    // Filters should be executed first.
    'weight' => -10,
  );

  return $callbacks;
}

/**
 * Helper to define supported entity types.
 */
function search_api_noindex_supported_entity_types() {
  return array('node');
}

/**
 * Helper to check if type is supported.
 */
function search_api_noindex_supported($entity_type = 'node') {
  return in_array($entity_type, search_api_noindex_supported_entity_types());
}

/**
 * Helper to check if the data alter callback is enabled for content type.
 */
function search_api_noindex_is_enabled($bundle) {
  $index_list = search_api_index_load_multiple(FALSE);
  foreach ($index_list as $index) {
    $data_alter_callbacks = !empty($index->options['data_alter_callbacks']) ? $index->options['data_alter_callbacks'] : array();
    // Check if noindex filter is enabled.
    if (isset($data_alter_callbacks['search_api_noindex_alter_noindex_filter'])) {
      $noindex_enabled = $data_alter_callbacks['search_api_noindex_alter_noindex_filter']['status'];

      // Check status Search API filter 'Bundle filter'.
      if ($index->item_type == 'node' && isset($data_alter_callbacks['search_api_alter_bundle_filter'])) {
        $filter_bundle_enabled = $data_alter_callbacks['search_api_alter_bundle_filter']['status'];
        if ($noindex_enabled && $filter_bundle_enabled) {
          $bundles = $data_alter_callbacks['search_api_alter_bundle_filter']['settings'];
          // $bundles['default'] => 1 = All but those from one of the selected bundles
          // $bundles['default'] => 0 = Only those from the selected bundles
          return $bundles['default'] xor in_array($bundle, $bundles['bundles']);
        }
      }

      // @Todo support user role filter.
    }
  }
}

/**
 * Helper function to check if entity is in table.
 */
function search_api_noindex_has_entity($entity, $type = 'node') {
  list($id) = entity_extract_ids($type, $entity);
  $query = db_select('search_api_noindex_data', 's')
    ->condition('entity_type', $type)
    ->condition('entity_id', $id);
  $query->addExpression('count(*)', 'counts');
  return $query->execute()->fetchColumn() > 0;
}

/**
 * Helper function to get id's of nodes that are supported.
 */
function search_api_noindex_get_ids(array $entity_ids, $type = 'node') {
  $query = db_select('search_api_noindex_data', 's')
    ->condition('entity_type', $type)
    ->condition('entity_id', $entity_ids, 'IN')
    ->fields('s', array('entity_id'));
  return $query->execute()->fetchCol();
}

/**
 * Insert entity from search_api_noindex().
 */
function search_api_noindex_insert($entity, $type = 'node') {
  list($id) = entity_extract_ids($type, $entity);
  db_merge('search_api_noindex_data')
    ->key(array('entity_type' => $type, 'entity_id' => $id))
    ->fields(array(
      'entity_type' => $type,
      'entity_id' => $id,
    ))
    ->execute();
}

/**
 * Delete entity from search_api_noindex().
 */
function search_api_noindex_delete($entity, $type = 'node') {
  list($id) = entity_extract_ids($type, $entity);
  db_delete('search_api_noindex_data')
    ->condition('entity_type', $type)
    ->condition('entity_id', $id)
    ->execute();
}
